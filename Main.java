public class Main {

    public static void main(String[] args) {
        System.out.println(Calculator.add(9.45, 2.1));
        System.out.println(Calculator.subtract(95.23, 4.1));
        System.out.println(Calculator.multiply(45.02, 123.05));
        System.out.println(Calculator.divide(9.0, 0.0));
    }
}
